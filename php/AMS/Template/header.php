<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admission Management System</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!-- Navigation Bar -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">AMS</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="main-navbar">
                <ul class="nav navbar-nav">
                    <li class="<?php if ($page == Home) echo active; ?>"><a href="index.php">Home</a></li>
                    <?php if(isset($_SESSION['user_login'])) { ?>
                    <li class="<?php if ($page == Apply) echo active; ?>"><a href="apply.php">Apply</a></li>
                    <li class="<?php if ($page == Apply) echo active; ?>"><a href="merit.php">merit</a></li>
                    <li class="<?php if ($page == Apply) echo active; ?>"><a href="timetable.php">Time Table</a></li>
                    <?php } ?>
                    <li class="<?php if ($page == About) echo active; ?>"><a href="about.php">About Us</a></li>
                   
                    
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php 
                    if (! isset($_SESSION['user_login'])) { ?>
                    <li class="<?php if ($page == Login) echo active; ?>"><a href="login.php">LogIn</a></li>
                    <li class="<?php if ($page == Signup) echo active; ?>"><a href="signup.php">SignUp</a></li>
                    <?php } else { ?>
                    <li><a href="logout.php">Logout</a></li>
                    <?php } ?>
                </ul>
            </div> <!-- /.navbar-collapse -->
        </div> <!-- /.container-fluid -->
    </nav> <!-- End navigation Bar -->