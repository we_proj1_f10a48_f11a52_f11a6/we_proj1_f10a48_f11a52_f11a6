<?php
session_start();
$page = "Login";
$error = "";
require "Template/header.php";
if(isset($_POST['submit'])){
    $user = $_POST['username'];
    $pass = $_POST['password'];
    
    require "db.php";
    
    $sql = "SELECT username, password FROM users WHERE username = '$user' AND password = '$pass' ";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        
        $_SESSION['user_login'] = $user;
        if($user=="admin")
        {
            header('location: admin.php');
        }
        header('Location: index.php');
    } else {
        $error = "Username or password is incorrect";
    }
    mysqli_close($conn);
}

?>

    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <p class="text-center">LogIn to the AMS</p>
                    </div>
                    <div class="panel-body col-sm-10 col-sm-offset-1">
                        <form action="" method="post" class="form-horizontal">
                            <div class="form-group input-group">
                                <input type="text" name="username" class="form-control" placeholder="Enter Username" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                            </div>
                            <div class="form-group input-group">
                                <input type="password" name="password" class="form-control" placeholder="Enter Password" required>
                                <span class="input-group-addon"><span class=" glyphicon glyphicon-eye-open"></span></span>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" value="LogIn" class="form-control btn btn-primary">
                            </div>
                        </form>
                        <p class="text-center text-warning"><?php echo $error; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require "Template/footer.php"; ?>