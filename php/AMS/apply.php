<?php
session_start();
$page = "Home";
$success = "";
$error = "";

if(isset($_SESSION['user_login'])) {
    if(isset($_POST['submit'])) {
        $user = $_POST['a_name'];
        $gardian = $_POST['g_name'];
        $dob = $_POST['dob'];
        $cnic =$_POST['cnic'];
        $nan = $_POST['nationality'];
        $gender = $_POST['gender'];
        $mobile = $_POST['mobile'];
        $phone = $_POST['phone'];
        $address = $_POST['address'];
        $m_marks = $_POST['m_marks'];
        $m_rollno = $_POST['m_rollno'];
        $m_board = $_POST['m_board'];
        $i_marks = $_POST['i_marks'];
        $i_rollno = $_POST['i_rollno'];
        $i_board = $_POST['i_board'];
        $program = $_POST['program'];
        if($program=="ms")
            $bs_marks=$_POST['bs_marks'];
        else
            $bs_marks=0;
        
        $bs_rollno = $_POST['bs_rollno'];
        $bs_board = $_POST['bs_board'];
        $hafiz = $_POST['hafiz'];
        
        if($program=="bs")
        {
            if($hafiz=="yes")
                $merit = 0.5*($i_marks+20) + 0.5*$m_marks;
            else
                $merit = 0.5*$i_marks + 0.5*$m_marks;
        }
        else
        {
            if($hafiz=="yes")
                $merit = 0.5*($bs_marks+20) + 0.25*$i_marks + 0.25*$m_marks;
            else
                $merit = 0.5*$bs_marks + 0.25*$i_marks + 0.25*$m_marks;
        }
        
        require "db.php";
        
        $una = $_SESSION['user_login'];
        
        
        $q = "SELECT id FROM users WHERE username = '$una' ";
        $res = mysqli_query($conn, $q);
        if (mysqli_num_rows($res) > 0) {
            $r = mysqli_fetch_assoc($res);
            $uid = $r['id'];
        }
        mysqli_close($conn);
        
        
        require "db.php";
        
         $sql = "INSERT INTO form(a_name,g_name,dob,cnic,nationality,gender,mobile,phone,address,m_marks,m_rollno,m_board,i_marks,i_rollno,i_board,bs_marks,bs_rollno,bs_board,program,hafiz,merit,u_id) VALUES ('$user','$gardian','$dob','$cnic','$nan','$gender','$mobile','$phone','$address',$m_marks,$m_rollno,'$m_board',$i_marks,$i_rollno,'$i_board',$bs_marks,'$bs_rollno','$bs_board','$program','$hafiz','$merit',$uid)";
        
    
    if (mysqli_query($conn, $sql)) {
    $success =  "Regsiter successfully";
} else {
    $error = "Error! Something wrong while filling form. Try after some time.";
}
        mysqli_close($conn);
    }
}
else {
    header('Location: index.php');
}


?>
    <?php require "Template/header.php"; ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">



                    <form method="post" action="">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <input type="text" name="a_name" class="form-control" placeholder="Applicant Name" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <input type="text" name="g_name" class="form-control" placeholder="Guardian name" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <input type="text" name="dob" class="form-control" placeholder="Date of Birth " required>
                            </div>
                            <div class="form-group col-sm-6">
                                <input type="text" name="cnic" class="form-control" placeholder="CNIC / B-Form " required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-sm-6">
                                <input type="text" name="nationality" class="form-control" placeholder="Nationality">
                            </div>
                            <div class="form-group col-sm-6">
                                <select name="gender" class="form-control">
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <input type="text" name="mobile" class="form-control" placeholder="Mobile Number" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <input type="text" name="phone" class="form-control" placeholder="Phone Number">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <input type="text" name="address" class="form-control" placeholder="Postal Address" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <input type="text" name="m_marks" class="form-control" placeholder="Matric Marks" required>
                            </div>
                            <div class="form-group col-sm-4">
                                <input type="text" name="m_rollno" class="form-control" placeholder="Matric Roll No" required>
                            </div>
                            <div class="form-group col-sm-4">
                                <input type="text" name="m_board" class="form-control" placeholder="Matric Board Name" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-4">
                                <input type="text" name="i_marks" class="form-control" placeholder="Intermediate Marks" required>
                            </div>
                            <div class="form-group col-sm-4">
                                <input type="text" name="i_rollno" class="form-control" placeholder="Intermediate Roll No" required>
                            </div>
                            <div class="form-group col-sm-4">
                                <input type="text" name="i_board" class="form-control" placeholder="Intermediate Board Name" required>
                            </div>

                            <div class="form-group col-sm-4">
                                <input type="text" name="bs_marks" class="form-control" placeholder="BS/BA Marks" required>
                            </div>
                            <div class="form-group col-sm-4">
                                <input type="text" name="bs_rollno" class="form-control" placeholder="BS/BA Roll No" required>
                            </div>
                            <div class="form-group col-sm-4">
                                <input type="text" name="bs_board" class="form-control" placeholder="BS/BA Board or Uni Name" required>
                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <select name="program" class="form-control">
                                    <option value="bs">BS Program</option>
                                    <option value="ms">MS Program</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <select name="hafiz" class="form-control">
                                    <option value="no">Hafiz No</option>
                                    <option value="yes">Hafiz Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="form-control btn btn-primary">
                        </div>


                    </form>
                    <p class="text-success text-center">
                        <?php echo $success; ?>
                    </p>
                    <p class="text-danger text-center">
                        <?php echo $error; ?>
                    </p>
                </div>
            </div>
        </div>
           <?php require "Template/footer.php"; ?>