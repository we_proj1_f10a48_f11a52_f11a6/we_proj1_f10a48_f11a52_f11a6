<?php
$page = "Signup";
$success = "";
$error = "";
if(isset($_POST['submit'])) {
    
    $user = $_POST['username'];
    $email = $_POST['email'];
    $pass = $_POST['password'];
    
    require "db.php";
    
    $sql = "INSERT INTO users (username, email, password)
            VALUES ('$user', '$email', '$pass')";

if (mysqli_query($conn, $sql)) {
    $success =  "Regsiter successfully";
} else {
    $error = "Error! Something wrong while registering user. Try after some time.";
}

mysqli_close($conn);
}

require "Template/header.php";
?>

<div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <p class="text-center">SignIn to the AMS</p>
                    </div>
                    <div class="panel-body col-sm-10 col-sm-offset-1">
                        <form action="" method="post" class="form-horizontal">
                            <div class="form-group input-group">
                                <input type="text" name="username" class="form-control" placeholder="Enter Username" required>
                                <span class="input-group-addon"><span class=" glyphicon glyphicon-user"></span></span>
                            </div>
                            <div class="form-group input-group">
                                <input type="email" name="email" class="form-control" placeholder="Enter Email" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                            </div>
                            <div class="form-group input-group">
                                <input type="password" name="password" class="form-control" placeholder="Enter Password" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span></span>
                            </div>
                            <div class="form-group input-group">
                                <input type="password" name="password_confirmed" class="form-control" placeholder="Repeat Password" required>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" value="Register" class="form-control btn btn-primary">
                            </div>
                        </form>
                            <p class="text-success text-center"><?php echo $success; ?></p>
                            <p class="text-danger text-center"><?php echo $error; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require "Template/footer.php"; ?>