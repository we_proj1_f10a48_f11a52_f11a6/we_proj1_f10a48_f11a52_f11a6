-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2015 at 06:50 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ams`
--

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE IF NOT EXISTS `form` (
  `id` int(10) unsigned NOT NULL,
  `a_name` varchar(64) NOT NULL,
  `g_name` varchar(64) NOT NULL,
  `dob` varchar(20) NOT NULL,
  `cnic` varchar(64) NOT NULL,
  `nationality` varchar(64) NOT NULL,
  `gender` varchar(64) NOT NULL,
  `mobile` varchar(64) NOT NULL,
  `phone` varchar(64) NOT NULL,
  `address` varchar(64) NOT NULL,
  `m_marks` int(5) NOT NULL,
  `m_rollno` int(8) NOT NULL,
  `m_board` varchar(64) NOT NULL,
  `i_marks` int(5) NOT NULL,
  `i_rollno` int(8) NOT NULL,
  `i_board` varchar(64) NOT NULL,
  `bs_marks` int(6) NOT NULL,
  `bs_rollno` varchar(64) NOT NULL,
  `bs_board` varchar(64) NOT NULL,
  `program` varchar(64) NOT NULL,
  `hafiz` varchar(64) NOT NULL,
  `merit` double NOT NULL,
  `u_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form`
--

INSERT INTO `form` (`id`, `a_name`, `g_name`, `dob`, `cnic`, `nationality`, `gender`, `mobile`, `phone`, `address`, `m_marks`, `m_rollno`, `m_board`, `i_marks`, `i_rollno`, `i_board`, `bs_marks`, `bs_rollno`, `bs_board`, `program`, `hafiz`, `merit`, `u_id`) VALUES
(34, 'm.taqi', 'Mohammad Birmani', '2-3-2015', '34546546456', 'pakistani', 'male', '45465465657567657', '5464563', 'sdkfshd nkdfgnkg knfkg jerkgnerkn.erk', 700, 1234, 'bise d.g.khan', 900, 4321, 'bise multan', 0, '0', 'mjcxjk', 'bs', 'no', 800, 7);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `username` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(7, 'rana', 'rana@gmail.com', '123'),
(8, 'rana', 'rana@gmail.com', '123'),
(9, 'admin', 'admin@gmail.com', 'admin'),
(10, 'admin', 'admin@gmail.com', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign_key_users` (`u_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `form`
--
ALTER TABLE `form`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `form`
--
ALTER TABLE `form`
  ADD CONSTRAINT `foreign_key` FOREIGN KEY (`u_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
